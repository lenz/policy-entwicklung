# Policy ID: 013

## BSI-Anforderungen

### SYS.1.6.A17
*Plattformbetreiber* **MUSS** ​sicherstellen, dass die Ausführung der Container-Runtime und von den instanziierten Containern als root User nicht möglich ist. Ausnahmen hiervon sind Container, welche Aufgaben des Host-Systems übernehmen.

## SYS.1.6.A18
*Plattformbetreiber* **MUSS** sicherstellen, dass Container nicht unter Root-Rechten laufen.

## Formale Beschreibung

Die Ausführung von Containern als Root-User (UID 0) ist zu unterbinden.


## Technische Umsetzungsvorschläge (optional)

- Pod securityContext: runAsNonRoot: true && runAsUser > 0

## Technische Umsetzungen

- Kyverno: [IG BvC Policy](https://gitlab.opencode.de/ig-bvc/policy-entwicklung/richtlinien-umsetzung-kyverno/-/blob/master/policies/require-run-as-non-root.yaml)
- NeuVector: Link // Standardmäßig implementiert (mit Link)
- StackRox:
  - [Standardmäßig implementiert - UserID](https://github.com/stackrox/stackrox/blob/master/pkg/defaults/policies/files/root_user.json)
  - [Standardmäßig implementiert - Process](https://github.com/stackrox/stackrox/blob/master/pkg/defaults/policies/files/process_uid_zero.json)
  - [IG BvC Policy - UserID](https://gitlab.opencode.de/ig-bvc/policy-entwicklung/richtlinien-umsetzung-stackrox/-/blob/main/policies/require-run-as-nonroot-image-user.json)
  - [IG BvC Policy - Process](https://gitlab.opencode.de/ig-bvc/policy-entwicklung/richtlinien-umsetzung-stackrox/-/blob/main/policies/require-run-as-nonroot-process.json)
