# Policy ID: 014

## BSI-Anforderungen

### SYS.1.6.A17
*Softwarebetreiber* **MUSS** die Privilegien für Container auf das erforderliche Minimum begrenzen.


## Formale Beschreibung

Die Ausführung von Containern mit Linux-Capabilities ist standardmäßig zu unterbinden. Beim Start eines Containern ist die Entfernung aller Capabilities explizit durchzuführen.

## Technische Umsetzungsvorschläge (optional)

- PodsecurityContext: capabilities.drop.ALL

## Technische Umsetzungen

- Kyverno: Link // Standardmäßig implementiert (mit Link)
- NeuVector: Link // Standardmäßig implementiert (mit Link)
- StackRox:
  - [Standardmäßig implementiert - Alert on CAP_SYS_ADMIN](https://github.com/stackrox/stackrox/blob/master/pkg/defaults/policies/files/cap_sys_admin.json)
  - [IG BvC Policy - Alert on CAP_SYS_ADMIN](https://gitlab.opencode.de/ig-bvc/policy-entwicklung/richtlinien-umsetzung-stackrox/-/blob/main/policies/disallow-cap-sys_admin.json)
