# Policy ID: 015

## BSI-Anforderungen

### SYS.1.6.A17
*Softwarebetreiber* **MUSS** die Konfiguration so umsetzen, dass keine erweiterten Privilegien angefordert werden. Ausnahmen hiervon sind Container, welche Aufgaben des Host-Systems übernehmen.


## Formale Beschreibung

Es ist sicherzustellen, dass Container nicht im privileged-Mode ausgeführt werden können.


## Technische Umsetzungsvorschläge (optional)

- Pod securityContext: privileged=false

## Technische Umsetzungen

- Kyverno: [IG BvC Policy](https://gitlab.opencode.de/ig-bvc/policy-entwicklung/richtlinien-umsetzung-kyverno/-/blob/master/policies/disallow-privileged-containers.yaml)
- NeuVector: Link // Standardmäßig implementiert (mit Link)
- StackRox:
  - [Standardmäßig implementiert](https://github.com/stackrox/stackrox/blob/master/pkg/defaults/policies/files/privileged.json)
  - [IG BvC](https://gitlab.opencode.de/ig-bvc/policy-entwicklung/richtlinien-umsetzung-stackrox/-/blob/main/policies/disallow-privileged.json)