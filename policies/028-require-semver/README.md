# Policy ID: 028

## BSI-Anforderungen

### SYS.1.6.A6

Es **MÜSSEN** eindeutige Versionsnummern vergeben sein.


## Formale Beschreibung

Images müssen nach den Regeln des "Semantic Versioning" (https://semver.org/) versioniert sein.

## Technische Umsetzungsvorschläge (optional)

- Prüfung Tags des Formats "1.5.4-alpha" oder 1.2.3+beta (Regex: `"^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$"`) [Quelle](https://semver.org/#is-there-a-suggested-regular-expression-regex-to-check-a-semver-string)

## Technische Umsetzungen

- Kyverno: Link // Standardmäßig implementiert (mit Link)
- NeuVector: Link // Standardmäßig implementiert (mit Link)
- StackRox: Link // Standardmäßig implementiert (mit Link)
