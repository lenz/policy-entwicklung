# Policy ID: 020

## BSI-Anforderungen

### APP.4.4.A9
*Softwarebetreiber* **MUSS** ​Berechtigungen für Service-Accounts so vergeben, dass Pods welche keinen Service-Account benötigen, diese nicht einsehen können und keinen Zugriff auf entsprechende Token haben.

## Formale Beschreibung

Es muss nach Least-Privilege-Prinzip entschieden werden, ob eine Anwendung einen Service-Account für Anfragen an den Kubernetes-API-Server benötigt. Benötigt eine Anwendung keinen Zugriff, ist der Zugriff auf den Service-Account-Token im Container zu unterbinden.

## Technische Umsetzungsvorschläge (optional)

- AdmissionControl in Pod: Prüfung ob "automountServiceAccountToken" gesetzt

## Technische Umsetzungen

- Kyverno: Link // Standardmäßig implementiert (mit Link)
- NeuVector: Link // Standardmäßig implementiert (mit Link)
- StackRox:
  - [Standardmäßig implementiert - Automount default Serviceaccounttoken](https://github.com/stackrox/stackrox/blob/master/pkg/defaults/policies/files/automount_service_account_token.json)
  - [IG BvC - Automount default Serviceaccounttoken](https://gitlab.opencode.de/ig-bvc/policy-entwicklung/richtlinien-umsetzung-stackrox/-/blob/main/policies/disallow-automount-satoken.json)
