# Policy ID: 000

## BSI-Anforderungen

### APP.4.4.A??
*Softwarebetreiber* **MUSS** dies tun.

### SYS.1.6.A??
*Plattformbetreiber* **SOLLTE** das tun.

### NET...?


## Formale Beschreibung

Möglichst genaue Beschreibung der zu blockierenden Funktionalität, mit Spielraum für technische Umsetzung (z.B. ssh-Verbindungen in Container sollten unterbunden werden).

## Technische Umsetzungsvorschläge (optional)

- z.B. Admission Control, Prüfung zur Laufzeit...

## Technische Umsetzungen

- Kyverno: Link // Standardmäßig implementiert (mit Link)
- NeuVector: Link // Standardmäßig implementiert (mit Link)
- StackRox: Link // Standardmäßig implementiert (mit Link)
