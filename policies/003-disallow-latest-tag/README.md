# Policy ID: 003

## BSI-Anforderungen

### SYS.1.6.A6
*Plattformbetreiber* **SOLLTE** ​​automatisierte Policies implementieren, die die Herkunft, Vertrauenswürdigkeit und Integrität der Images prüfen und durchsetzen.


## Formale Beschreibung

Die Nutzung des "latest" Image-Tags sollte unterbunden werden. Stattdessen sollten klare Versionsnummern angegeben werden.

## Technische Umsetzungsvorschläge (optional)

- z.B. Admission Control, Prüfung zur Laufzeit...

## Technische Umsetzungen

- Kyverno: [IG BvC Policy](https://gitlab.opencode.de/ig-bvc/policy-entwicklung/richtlinien-umsetzung-kyverno/-/blob/master/policies/disallow-latest-tag.yaml)
- NeuVector: Link // Standardmäßig implementiert (mit Link)
- StackRox: [Standardmäßig implementiert](https://github.com/stackrox/stackrox/blob/master/pkg/defaults/policies/files/latest_tag.json)
