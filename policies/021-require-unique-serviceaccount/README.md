# Policy ID: 021

## BSI-Anforderungen

### APP.4.4.A9
*Softwarebetreiber* **MUSS** sicher stellen, dass Pods für unterschiedliche Verfahren/Anwendungen unter jeweils eigenen Service-Accounts betrieben werden.

## Formale Beschreibung

Es muss sichergestellt werden, dass Serviceaccounts klar den Pods EINES Deployments zugeordnet werden können.

## Technische Umsetzungsvorschläge (optional)

- AdmissionControl bei Pod: Anfrage an API-Server, ob schon ein Pod eines anderen Deployments den angeforderten ServiceAccount nutzt

## Technische Umsetzungen

- Kyverno: Link // Standardmäßig implementiert (mit Link)
- NeuVector: Link // Standardmäßig implementiert (mit Link)
- StackRox: Keine Prüfung möglich, ob Serviceaccount ausschließlich einem Pod zugeordnet ist (4.1).
