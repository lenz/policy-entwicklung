# Policy ID: 017

## BSI-Anforderungen

### SYS.1.6.A18
*Plattformbetreiber* **MUSS** ​einen Bereich von User- und Group-ID's zur Verwendung in Containern bereitstellen, die keine Berechtigungen auf die System- und Datenbereiche der Container-Hosts besitzen.

## Formale Beschreibung

Container sollten ausschließlich mit einer UID 2000 über gestartet werden, um Überlappungen mit Accounts auf dem Host-System zu vermeiden.

## Technische Umsetzungsvorschläge (optional)

- runAsUser > 2000?

## Technische Umsetzungen

- Kyverno: Link // Standardmäßig implementiert (mit Link)
- NeuVector: Link // Standardmäßig implementiert (mit Link)
- StackRox: Keine UID Prüfung für Deployments möglich (3.73).
