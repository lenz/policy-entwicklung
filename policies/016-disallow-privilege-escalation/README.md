# Policy ID: 016

## BSI-Anforderungen

### SYS.1.6.A17
*Plattformbetreiber* **MUSS** ​sicherstellen, dass die Nutzung erweiterter Rechte bei der Ausführung der Container-Runtime und von den instanziierten Containern verhindert wird. Ausnahmen hiervon sind Container, welche Aufgaben des Host-Systems übernehmen.

## Formale Beschreibung

Das Setzen von Set-UID oder Set-GID Bits auf Child-Prozesse eines Containers sollte unterbunden werden.

## Technische Umsetzungsvorschläge (optional)

- securityContext: allowPrivilegeEscalation=false

## Technische Umsetzungen

- Kyverno: [IG BvC Policy](https://gitlab.opencode.de/ig-bvc/policy-entwicklung/richtlinien-umsetzung-kyverno/-/blob/master/policies/deny-privilege-escalation.yaml)
- NeuVector: Link // Standardmäßig implementiert (mit Link)
- StackRox:
  - [Standardmäßig implementiert](https://github.com/stackrox/stackrox/blob/master/pkg/defaults/policies/files/privilege_escalation.json)
  - [IG BvC](https://gitlab.opencode.de/ig-bvc/policy-entwicklung/richtlinien-umsetzung-stackrox/-/blob/main/policies/disallow-privilege-escalation.json)
