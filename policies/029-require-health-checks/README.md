# Policy ID: 029

## BSI-Anforderungen

### APP.4.4.A11

*Softwarebetreiber* **MUSS** die vom Softwarelieferant beschriebenen Health-Checks in die Plattform einbinden.

### SYS.1.6.A9

*Softwarelieferant* **SOLLTE** Schnittstellen zur Überwachung des Betriebszustands der Container vorsehen, damit ein automatischer Restart ermöglicht werden kann.

## Formale Beschreibung

Eine Policy muss sicherstellen, dass Container regelmäßig mittels Health-Checks auf ihre Lauffähigkeit geprüft werden.

## Technische Umsetzungsvorschläge (optional)

- Admission Control, Prüfung Kubernetes Deployment auf spec.containers...
- Prüfung zur Laufzeit, ob in regelmäßigen Abständen eine Verbindung zum Pod aufgebaut wird.

## Technische Umsetzungen

- Kyverno: https://gitlab.opencode.de/ig-bvc/policy-entwicklung/richtlinien-umsetzung-kyverno/-/blob/master/policies/require-health-and-liveness-check.yaml
- NeuVector: tbd
- StackRox: tbd
