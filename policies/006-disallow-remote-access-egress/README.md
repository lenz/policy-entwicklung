# Policy ID: 006

## BSI-Anforderungen

### SYS.1.6.A16
*Softwarebetreiber* **MUSS** ​sicherstellen, dass aus einem Container heraus keine administrativen Zugriffe auf den Container-Host erfolgen können.


## Formale Beschreibung

Es muss sichergestellt werden, dass aus einem Container heraus keine Netzwerkverbindungen über typische Fernwartungsports geöffnet werden können.

## Technische Umsetzungsvorschläge (optional)

- Default-Deny-NetworkPolicy + AdmissionControl: Verbieten des Erstellens vom NetworkPolicy-Objekten, die Egress Traffic auf z.B. Port 22 freischalten würden.

## Technische Umsetzungen

- Kyverno: Link // Standardmäßig implementiert (mit Link)
- NeuVector: Link // Standardmäßig implementiert (mit Link)
- StackRox: Nicht Pruefbar (3.73)
