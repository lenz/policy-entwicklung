# Policy ID: 023

## BSI-Anforderungen

### APP.4.4.A14
*Softwarebetreiber* **MUSS** ​​das Verteilungsschema für die Pods definieren. ​Die Nodes müssen mindestens nach folgenden Aufgaben unterschieden und getrennt werden:
- Bastion-Nodes zur Realisierung von ingress und egress
- Anwendungs-Nodes zum Betrieb der Pods für die Anwendungen
- Speicher-Nodes zur Bereitstellung der Speicherlösungen


## Formale Beschreibung

Es muss sichergestellt werden, dass Ingress-Container auf den dafür vorgesehenen Nodes ausgerollt werden.

## Technische Umsetzungsvorschläge (optional)

- Affinity-Rules

## Technische Umsetzungen

- Kyverno: Link // Standardmäßig implementiert (mit Link)
- NeuVector: Link // Standardmäßig implementiert (mit Link)
- StackRox: Keine Prüfung von Affinity Rules möglich (3.73)
