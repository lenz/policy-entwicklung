# Policy ID: 009

## BSI-Anforderungen

### SYS.1.6.A19
*Softwarelieferant / Softwarebetreiber* **SOLLTE** keine lokalen Speicher der Workernodes benutzen.

## Formale Beschreibung

Die Nutzung von Volumes des Typs "hostPath" sollte unterbunden werden.

## Technische Umsetzungsvorschläge (optional)

- Admission Control: Statische Prüfung auf Pod .spec.volumes.hostPath

## Technische Umsetzungen

- Kyverno: [IG BvC Policy](https://gitlab.opencode.de/ig-bvc/policy-entwicklung/richtlinien-umsetzung-kyverno/-/blob/master/policies/disallow-host-path.yaml)
- NeuVector: Link // Standardmäßig implementiert (mit Link)
- StackRox: [IG BvC Policy](https://gitlab.opencode.de/ig-bvc/policy-entwicklung/richtlinien-umsetzung-stackrox/-/blob/main/policies/disallow-hostpath.json)
